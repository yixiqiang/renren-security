package io.renren.modules.vue.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@RequestMapping("/vue")
@Controller
public class VueController {
	
	@RequestMapping("/vue.html")
	public String toPage() {
		
		return "views/index/vueIndex";
		
	}
 
	
	@RequestMapping("/header.html")
	public ModelAndView head() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("views/index/Header.vue");
		return mav;
	}

	@RequestMapping("/sidebar.html")
	public ModelAndView side() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("views/index/Sidebar.vue");
		return mav;
	}
	@RequestMapping("/tags.html")
	public ModelAndView tags() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("views/index/Tags.vue");
		return mav;
	}
}
